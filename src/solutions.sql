--A:
SELECT * FROM artists WHERE name LIKE "%d%";

--B:
SELECT * FROM songs WHERE length < 230;

--C:
SELECT album_title, song_name, length FROM albums
    JOIN songs ON albums.id = songs.album_id;

--D:
SELECT * FROM albums WHERE album_title LIKE "%a%";
SELECT * FROM artists
    JOIN albums on artists.id = albums.artist_id;

--E:
SELECT * FROM albums ORDER BY album_title DESC LIMIT 4;

--F:
SELECT * FROM albums
    JOIN songs ON albums.id = songs.album_id;
SELECT * FROM albums ORDER BY album_title DESC;
SELECT * FROM songs ORDER BY song_name ASC;